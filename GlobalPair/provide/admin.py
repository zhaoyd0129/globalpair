from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from .models import Provider, Service


# Register your models here.
# class ProvideInline(admin.StackedInline):
#     model = Provide


class ProviderAdmin(admin.ModelAdmin):
    list_display = ('id', 'account', 'company',
                    'amount_request_pushed',
                    'amount_market_research',
                    'amount_industry_consultation',
                    'amount_translation_service',
                    'amount_overseas_recruitment',
                    'amount_office_space_Rental',
                    'amount_legal_incorporation',
                    'amount_tax_accounting',
                    'amount_business_travel')
    fields = (('account', 'company', 'amount_request_pushed'),
              ('service_types','target_market'),
              ('amount_market_research', 'amount_industry_consultation',
               'amount_translation_service', 'amount_overseas_recruitment'),
              ('amount_office_space_Rental', 'amount_legal_incorporation',
               'amount_tax_accounting', 'amount_business_travel'),
    )

    # inlines = [ProvideInline]


class ServiceAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'provider',
        'service_title',
        'service_type',
        'service_detail',
        'quotation',
        'date_published',
        'requester_accepted',
        'provider_cancelled',
    )

admin.site.register(Provider, ProviderAdmin)
admin.site.register(Service, ServiceAdmin)

# Register your models here.
