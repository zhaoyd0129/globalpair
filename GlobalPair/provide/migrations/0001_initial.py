# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Provider',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount_request_pushed', models.PositiveSmallIntegerField(default=0, verbose_name='request pushed number')),
                ('amount_market_research', models.PositiveSmallIntegerField(default=0, verbose_name='Market Research')),
                ('amount_industry_consultation', models.PositiveSmallIntegerField(default=0, verbose_name='Industry Consultation')),
                ('amount_translation_service', models.PositiveSmallIntegerField(default=0, verbose_name='Translation Service')),
                ('amount_overseas_recruitment', models.PositiveSmallIntegerField(default=0, verbose_name='Overseas Recruitment')),
                ('amount_office_space_Rental', models.PositiveSmallIntegerField(default=0, verbose_name='Office Space Rental')),
                ('amount_legal_incorporation', models.PositiveSmallIntegerField(default=0, verbose_name='Legal & Incorporation')),
                ('amount_tax_accounting', models.PositiveSmallIntegerField(default=0, verbose_name='Tax & Accounting')),
                ('amount_business_travel', models.PositiveSmallIntegerField(default=0, verbose_name='Business Trip Planning')),
                ('account', models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, verbose_name='account')),
                ('company', models.OneToOneField(null=None, blank=True, to='account.Company', verbose_name='company')),
                ('service_types', models.ManyToManyField(to='account.ServiceType', verbose_name='service types')),
                ('target_market', models.ManyToManyField(to='account.TargetMarket', verbose_name='target market')),
            ],
            options={
                'verbose_name': 'provider',
                'verbose_name_plural': 'providers',
            },
        ),
        migrations.CreateModel(
            name='Service',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('service_type', models.CharField(max_length=30, verbose_name='service type')),
                ('service_detail', models.TextField(max_length=200, verbose_name='detail')),
                ('quotation', models.PositiveIntegerField(verbose_name='quotation')),
                ('date_published', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date published')),
                ('requester_accepted', models.NullBooleanField(help_text='Is the request accepted by requester, when it istrue, it can not be deleted', verbose_name='requester accepted')),
                ('provider_cancelled', models.BooleanField(default=False, verbose_name='provider cancelled')),
                ('provider', models.ForeignKey(verbose_name='provider', to='provide.Provider')),
            ],
            options={
                'verbose_name': 'service',
                'verbose_name_plural': 'services',
            },
        ),
    ]
