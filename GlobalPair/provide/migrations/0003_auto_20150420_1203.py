# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('provide', '0002_service_service_title'),
    ]

    operations = [
        migrations.AlterField(
            model_name='service',
            name='service_title',
            field=models.CharField(default='', max_length=30, verbose_name='service title'),
        ),
    ]
