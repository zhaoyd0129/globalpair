from __future__ import unicode_literals
from account.models import Company, ServiceType, TargetMarket
from django.utils import timezone
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db import models

# Create your models here.
from request.models import Request, TARGET_MARKET, SERVICE_TYPE


class Provider(models.Model):
    """
    extra info of provider for  whole  provide, some statistic data
    """
    # need overwrite the widgets

    # extra info need as provider
    # accountUser = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, verbose_name=_('account user id'))
    account = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('account'), blank=True, null=True)

    # used for staff to break the relations between account and company one to one, we can think
    # those companies have no account, all though it is redundancy
    company = models.OneToOneField(Company, verbose_name=_('company'), blank=True, null=None)
    # service_types = models.CharField(_('services can provide'), max_length=1, choices=SERVICE_TYPE)
    # service_types = models.CharField(_('services can provide'), max_length=1, choices=SERVICE_TYPE)
    service_types = models.ManyToManyField(ServiceType, verbose_name=_('service types'))
    # target_market = models.CharField(_('target market'), max_length=2, choices=TARGET_MARKET)
    target_market = models.ManyToManyField(TargetMarket,verbose_name=_('target market'))

    # statistic info
    amount_request_pushed = models.PositiveSmallIntegerField(_('request pushed number'),default=0)
    amount_market_research = models.PositiveSmallIntegerField(_('Market Research'), default=0)
    amount_industry_consultation = models.PositiveSmallIntegerField(_('Industry Consultation'), default=0)
    amount_translation_service = models.PositiveSmallIntegerField(_('Translation Service'), default=0)
    amount_overseas_recruitment = models.PositiveSmallIntegerField(_('Overseas Recruitment'), default=0)
    amount_office_space_Rental = models.PositiveSmallIntegerField(_('Office Space Rental'), default=0)
    amount_legal_incorporation = models.PositiveSmallIntegerField(_('Legal & Incorporation'), default=0)
    amount_tax_accounting = models.PositiveSmallIntegerField(_('Tax & Accounting'), default=0)
    amount_business_travel = models.PositiveSmallIntegerField(_('Business Trip Planning'), default=0)

    class Meta:
        # permissions = ()
        verbose_name = _('provider')
        verbose_name_plural = _('providers')

    def __unicode__(self):
        if self.company:
            return self.company.company_name
        return str(self.id)


class Service(models.Model):
    """
    info of each provide
    you can't add it before providerProfile
    """
    provider = models.ForeignKey(Provider, verbose_name=_('provider'))
    # accountUser = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user email'), blank=True, null=True)
    service_title = models.CharField(_('service title'), max_length=30,default="")
    service_type = models.CharField(_('service type'), max_length=30)
    # service_type = models.ForeignKey(ServiceType, verbose_name=_('service type'), limit_choices_to={})
    service_detail = models.TextField(_('detail'), max_length=200)
    quotation = models.PositiveIntegerField(_('quotation'))
    # transaction value  default=provide.budget
    # transaction_value = models.PositiveIntegerField(_('transaction value'))
    date_published = models.DateTimeField(_('date published'), default=timezone.now)

    # when the provider submit a provider, it means it has made a multiple-to-one relationship to the request
    # request = models.ForeignKey(Request, verbose_name=_('request'), blank=True, null=True)
    # request = models.ForeignKey(Request, verbose_name=_('request'))

    # if the requester buy its service, it is true.
    # then the sum of one services is updated. otherwise,it will be deleted some times later.
    # we should provide the info to the provider
    requester_accepted = models.NullBooleanField(_('requester accepted'), blank=True, null=True,
                                                   help_text=_('Is the request accepted by requester, when it is'
                                                               'true, it can not be deleted'))
    provider_cancelled = models.BooleanField(_('provider cancelled'), default=False)

    class Meta:
        # permissions = ()
        verbose_name = _('service')
        verbose_name_plural = _('services')

    # can it add price?
    def __unicode__(self):
        if self.service_title:
            return self.service_title
        return str(self.id)
