from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from .models import Message, Order


# Register your models here.
class MessageInline(admin.StackedInline):
    model = Message


class OrderAdmin(admin.ModelAdmin):
    list_display = ('id',
                    'request', 'provider', 'service',
                    'date_pushed',
                    'request_accept', 'service',
                    'is_allowed_messages',
                    'service_accept',
                    'date_buy',
                    'pay_status',
                    'date_pay',
                    )
    fields = (
        ('request', 'provider','date_pushed'),
        ('request_accept', 'service'),
        ('is_allowed_messages', 'service_accept'),
        ('date_buy','pay_status', 'date_pay'),
    )

    inlines = [MessageInline]


class MessageAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'order',
        'message_text',
        'date_published',
    )

admin.site.register(Order, OrderAdmin)
admin.site.register(Message, MessageAdmin)

# Register your models here.
