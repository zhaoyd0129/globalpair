from __future__ import unicode_literals
from django.utils import timezone
from django.utils.translation import ugettext_lazy as _
from django.db import models

# Create your models here.
from provide.models import Service, Provider
from request.models import Request


class Order(models.Model):
    """
    This is the most important model, and may be accessed frequently.

    First push request to the providers
    Second provider choose Accept/Not Accept,then he should provide some info to the request
     if not delete the record, and the related Messages. this must do once time.
    Third requester do the same as provider does as above
    the records not be handled will be deleted some time later

    when should delete if before provide or request be deleted.

    """
    request = models.ForeignKey(Request, verbose_name=_('request'))

    # providerProfile is the symbol of provider
    provider = models.ForeignKey(Provider, verbose_name=_('provider'))
    date_pushed = models.DateTimeField(_('date pushed'), default=timezone.now)

    # once the status is changed it can't do it again
    request_accept = models.NullBooleanField(_('accept request'), blank=True, null=True)
    service = models.ForeignKey(Service, blank=True, null=True)
    # default not provider contact the requester firstly,changed when request sends messages,
    # when the deal finished ,it changes False.
    is_allowed_messages = models.BooleanField(_('is allowed contact?'), default=False)

    # before the request selecting, he can ask more detail
    # once you accept one provider, others will not. this is set in Request by provider_selected,
    # so does the provide, defined as is_passed_by_request
    service_accept = models.NullBooleanField(_('accept service'), blank=True, null=True)

    date_buy = models.DateTimeField(_('date buy'), default=timezone.now)
    pay_status = models.BooleanField(_('pay status'), default=False)
    # deposit=models.PositiveIntegerField(_('deposit'),default=0)
    date_pay = models.BooleanField(_('date pay'), default=False)

    class Meta:
        # permissions = ()
        verbose_name = _('order')
        verbose_name_plural = _('orders')
        unique_together = ('request', 'provider')

    def __unicode__(self):
        return str(self.id)


class Message(models.Model):
    order = models.ForeignKey(Order, verbose_name='order')
    message_text = models.TextField(_('message text'),max_length=500)
    date_published = models.DateTimeField(_('data published'), default=timezone.now)

    class Meta:
        # permissions = ()
        verbose_name = _('Message')
        verbose_name_plural = _('Messages')

    def __unicode__(self):
        return str(self.id)