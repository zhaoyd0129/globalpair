# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('request', '0001_initial'),
        ('provide', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Message',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('message_text', models.TextField(max_length=500, verbose_name='message text')),
                ('date_published', models.DateTimeField(default=django.utils.timezone.now, verbose_name='data published')),
            ],
            options={
                'verbose_name': 'Message',
                'verbose_name_plural': 'Messages',
            },
        ),
        migrations.CreateModel(
            name='Order',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('date_pushed', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date pushed')),
                ('request_accept', models.NullBooleanField(verbose_name='accept request')),
                ('is_allowed_messages', models.BooleanField(default=False, verbose_name='is allowed contact?')),
                ('service_accept', models.NullBooleanField(verbose_name='accept service')),
                ('date_buy', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date buy')),
                ('pay_status', models.BooleanField(default=False, verbose_name='pay status')),
                ('date_pay', models.BooleanField(default=False, verbose_name='date pay')),
                ('provider', models.ForeignKey(verbose_name='provider', to='provide.Provider')),
                ('request', models.ForeignKey(verbose_name='request', to='request.Request')),
                ('service', models.ForeignKey(blank=True, to='provide.Service', null=True)),
            ],
            options={
                'verbose_name': 'order',
                'verbose_name_plural': 'orders',
            },
        ),
        migrations.AddField(
            model_name='message',
            name='order',
            field=models.ForeignKey(verbose_name='order', to='order.Order'),
        ),
        migrations.AlterUniqueTogether(
            name='order',
            unique_together=set([('request', 'provider')]),
        ),
    ]
