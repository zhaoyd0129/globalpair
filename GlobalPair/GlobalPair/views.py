from django.shortcuts import render
from django.utils.translation import ugettext as _
import time


def home(request):
    t = time.localtime()
    n = t[6]
    weekdays = [_('Monday'), _('Tuesday'), _('Wednesday'), _('Thursday'), _('Friday'), _('Saturday'), _('Sunday')]
    # return HttpResponse(weekdays[n])
    return render(request, 'index.html', {'weekday': weekdays[n]})
