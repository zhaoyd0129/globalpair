from django.conf.urls import include, url
from django.contrib import admin

urlpatterns = [
    # Examples:
    # url(r'^$', 'GlobalPair.views.home', name='home'),
    # url(r'^blog/', include('blog.urls')),

    url(r'^$', 'GlobalPair.views.home', name='home'),

    url(r'^admin/', include(admin.site.urls)),
    url(r'^i18n/', include('django.conf.urls.i18n')),

    # your app
    url(r'^account/', include('account.urls', namespace='account')),
    url(r'^provide/', include('provide.urls', namespace='provide')),
    url(r'^request/', include('request.urls', namespace='request')),
]
