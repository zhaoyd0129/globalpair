# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
    ]

    operations = [
        migrations.RemoveField(
            model_name='company',
            name='account',
        ),
        migrations.AddField(
            model_name='account',
            name='company',
            field=models.ForeignKey(verbose_name='Company', blank=True, to='account.Company', null=True),
        ),
    ]
