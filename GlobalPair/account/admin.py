from __future__ import unicode_literals
from django.utils.translation import ugettext_lazy as _
from django.contrib import admin
from django.contrib.auth.admin import UserAdmin
from image_cropping import ImageCroppingMixin
from .models import Account, Company, ServiceType, TargetMarket
from .forms import UserCreationForm, UserChangeForm


# class CompanyInline(ImageCroppingMixin, admin.StackedInline):
#     model = Company


class AccountAdmin(UserAdmin):
    # The forms to add and change user instances
    form = UserChangeForm
    add_form = UserCreationForm

    fieldsets = (
        (None, {'fields': ('email', 'password')}),
        (_('Personal info'), {'fields': (('first_name', 'last_name'),
                                         ('email', 'phone_number'), ('country', 'city'),
                                         ('company',)
                                         )}),
        (_('Permissions'), {'fields': ('is_active', 'is_email_verified', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email', 'first_name', 'last_name', 'country','phone_number', 'password1', 'password2'),
        }),
    )
    list_display = ('id', 'email', 'full_name', 'phone_number', 'country',
                    'is_staff', 'date_joined', 'is_email_verified', 'company')
    list_filter = ('is_staff', 'is_superuser', 'is_active', 'groups')
    search_fields = ('email','first_name', 'last_name')
    ordering = ('email',)
    filter_horizontal = ('groups', 'user_permissions',)

    # inlines = [CompanyInline]


class CompanyAdmin(ImageCroppingMixin, admin.ModelAdmin):
    list_display = ('id', 'company_name', 'company_url', 'company_intro','number_of_employee' )


class ServiceTypeAdmin(admin.ModelAdmin):
    list_display = ('id', 'service_name')


class TargetMarketAdmin(admin.ModelAdmin):
    list_display = ('id', 'target_market')
# Now register the new UserAdmin...
admin.site.register(Account, AccountAdmin)
admin.site.register(Company, CompanyAdmin)
admin.site.register(ServiceType, ServiceTypeAdmin)
admin.site.register(TargetMarket, TargetMarketAdmin)
# admin.site.register(CompanyProfile)
