from __future__ import unicode_literals
import os
from image_cropping import ImageRatioField
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.utils import timezone
from django.db import models
from django.contrib.auth.models import (
    BaseUserManager, AbstractBaseUser, PermissionsMixin
)
from .constants import COUNTRY_CHOICES
from django.utils.timezone import now

# bit set
# 0001  requester
# 0010  provider
# 0100  personal
# 1000  company
ACCOUNT_TYPE = (
    (None, _('account type')),
    (1, _('requester')),
    (2, _('provider')),
    (4, _('personal')),
    (8, _('company')),
)


def custom_file_path(instance, filename):
    """
    Create unique filename and dir
    """
    filename = instance.company_name
    date = now().strftime("%Y/%m/%d/")
    return os.path.join("profiles/", date, filename)


class Company(models.Model):
    """
    company basic info
    """
    # accountUser = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, verbose_name=_('account user id'))
    # account = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('account'), blank=True, null=True)
    company_name = models.CharField(_('company name'), default='', max_length=20)
    company_logo = models.ImageField(_('company logo'), upload_to=custom_file_path, blank=True, null=True, max_length=50)
    company_url = models.CharField(_('company url'), max_length=100, blank=True)

    # For cropping the image
    cropping = ImageRatioField('company_logo', '200x200')
    company_intro = models.TextField(_('company introduction'), max_length=1000)
    # provider_services = models.PositiveIntegerField(_('services provided'), default=0)
    # demand_services = models.PositiveIntegerField(_('services demanded'))
    number_of_employee = models.PositiveIntegerField(_('number of employee'), blank=True, null=True)

    def __unicode__(self):
        return self.company_name

    class Meta:
        # permissions = ()
        verbose_name = _('company')
        verbose_name_plural = _('companies')


class AccountManager(BaseUserManager):
    """
    customer UserManager, two main function inherited must be implemented
    """
    def create_user(self, email, first_name, last_name, password, is_superuser=False, is_staff=False):
        """
        Creates and saves a User with the given email, date of
        birth and password.
        """
        now = timezone.now()
        if not email:
            raise ValueError(_('email must have an email address'))

        user = self.model(
            email=self.normalize_email(email),
            first_name=first_name,
            last_name=last_name,
            date_joined=now,
            is_superuser=is_superuser,
            is_staff=is_staff
        )

        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_superuser(self, email, first_name, last_name, password):
        """
        Creates and saves a superuser with the given email, date of
        birth and password.
        """
        user = self.create_user(email, first_name, last_name, password, True, True)
        # user.is_admin = True
        user.save(using=self._db)
        return user


class Account(AbstractBaseUser, PermissionsMixin):
    """
    custom User
    PermissionsMixin  A mixin class that adds the fields and methods necessary to support
    Django's Group and Permission model using the ModelBackend.
    """
    first_name = models.CharField(_('first name'), max_length=20, blank=False,
                                  error_messages={
                                      'blank': _('please input first name'),
                                      })
    last_name = models.CharField(_('last name'), max_length=20, blank=False,
                                 error_messages={
                                     'blank': _('please input last name'),
                                     })
    email = models.EmailField(_('email address'), unique=True, max_length=30)
    phone_number = models.CharField(_('phone number'), max_length=15, blank=True)
    date_joined = models.DateTimeField(_('date joined'), default=timezone.now)
    country = models.CharField(_('country'), max_length=2, choices=COUNTRY_CHOICES, blank=False,
                               error_messages={
                                   'blank': _('please select your country'),
                               })
    city = models.CharField(_('city'), max_length=20, blank=True)
    company = models.ForeignKey(Company, verbose_name=_('Company'), blank=True, null=True)
    is_email_verified = models.BooleanField(_('email verified'), default=False,
                                            help_text=_('Only the verified user can post a request or solution'))
    is_staff = models.BooleanField(_('staff status'), default=False,
                                   help_text=_('Designates whether the user can log into this admin '
                                               'site.'))
    is_active = models.BooleanField(_('active'), default=True,
                                    help_text=_('Designates whether this user should be treated as '
                                                'active. Unselect this instead of deleting accounts.'))

    account_type = models.PositiveSmallIntegerField(_('account type'), choices=ACCOUNT_TYPE, default=1)
    objects = AccountManager()

    USERNAME_FIELD = 'email'
    REQUIRED_FIELDS = ['first_name', 'last_name', ]

    class Meta:
        permissions = (
            ('view_Account', 'Can view account'),
        )
        verbose_name = _('account')
        verbose_name_plural = _('accounts')

    def get_full_name(self):
        # The user is identified by their email address
        return self.email

    def get_short_name(self):
        # The user is identified by their email address
        return self.email

    def get_name_full(self):
        """
        concat first name and last name
        :return:
        """
        return "%s %s" % (self.first_name, self.last_name)

    # property for function used in data present
    get_name_full.short_description = _("Full name")

    full_name = property(get_name_full)

    def __unicode__(self):              # __unicode__ on Python 2
        return self.email


class ServiceType(models.Model):

    service_name = models.CharField(_('service name'), max_length=30)

    class Meta:
        # permissions = (serivce type)
        verbose_name = _('service type')
        verbose_name_plural = _('services type')

    def __unicode__(self):
        return self.service_name


class TargetMarket(models.Model):

    target_market = models.CharField(_('target market'), max_length=30)

    class Meta:
        # permissions = ()
        verbose_name = _('target market')
        verbose_name_plural = _('target market')

    def __unicode__(self):
        return self.target_market
