from __future__ import unicode_literals
from account.models import TargetMarket, ServiceType
from django.conf import settings
from django.utils.translation import ugettext_lazy as _
from django.db import models
from django.utils import timezone

SERVICE_TYPE = (
    (None, _('Select A Service')),
    ('1', _('Market Research')),
    ('2', _('Industry Consultation')),
    ('3', _('Translation Service')),
    ('4', _('Overseas Recruitment')),
    ('5', _('Office Space Rental')),
    ('6', _('Legal & Incorporation')),
    ('7', _('Tax & Accounting')),
    ('8', _('Business Trip Planning')),
)

TARGET_MARKET = (
    (None, _('Select Market Target')),
    ('US', _('America')),
    ('EU', _('Europe')),
    ('SA', _('Southeast Asia')),
    ('IN', _('India')),
    ('CH', _('China')),
)
# Create your models here.


class Requester(models.Model):
    """
    extra info of requester for  whole  request, some statistic data
    can't be deleted by anyone
    """
    # accountUser = models.OneToOneField(settings.AUTH_USER_MODEL, primary_key=True, verbose_name=_('account user id'))
    account = models.OneToOneField(settings.AUTH_USER_MODEL, verbose_name=_('account'), blank=True, null=True)
    # companyProfile = models.OneToOneField(CompanyProfile, verbose_name=_('company id'),blank=True, null=None)
    # request_finished_amount = models.PositiveSmallIntegerField(_('finished'),
    #                                                            default=0,
    #                                                            help_text=_('including solved and unsolved'))
    # request_ongoing_amount = models.PositiveSmallIntegerField(_('ongoing'), default=0)
    # request_verify_amount = models.PositiveSmallIntegerField(_('verifying'), default=0)

    # statistic  sum of different request service type
    # Pending Disapproved In Progress Finished Canceled
    amount_market_research = models.PositiveSmallIntegerField(_('Market Research'), default=0)
    amount_industry_consultation = models.PositiveSmallIntegerField(_('Industry Consultation'), default=0)
    amount_translation_service = models.PositiveSmallIntegerField(_('Translation Service'), default=0)
    amount_overseas_recruitment = models.PositiveSmallIntegerField(_('Overseas Recruitment'), default=0)
    amount_office_space_Rental = models.PositiveSmallIntegerField(_('Office Space Rental'), default=0)
    amount_legal_incorporation = models.PositiveSmallIntegerField(_('Legal & Incorporation'), default=0)
    amount_tax_accounting = models.PositiveSmallIntegerField(_('Tax & Accounting'), default=0)
    amount_business_travel = models.PositiveSmallIntegerField(_('Business Trip Planning'), default=0)

    class Meta:
        # permissions = ()
        verbose_name = _('requester')
        verbose_name_plural = _('requester')

    def __unicode__(self):
        if self.account_id:
            return self.account.full_name
        return str(self.id)


class Request(models.Model):
    """
    info of each request
    You can't add it before RequestProfile
    """
    # accountUser = models.ForeignKey(settings.AUTH_USER_MODEL, verbose_name=_('user email'), blank=True, null=True)
    requester = models.ForeignKey(Requester, verbose_name=_('requester'))
    request_title = models.CharField(_('title'), max_length='100')
    # request_service_type = models.CharField(_('service type'), max_length=1, choices=SERVICE_TYPE)
    request_detail=models.TextField(_('request detail'), max_length=500)
    service_type = models.ForeignKey(ServiceType, verbose_name=_('service type'))

    # target_market support multiple select, this is not
    # target_market = models.CharField(_('target market'), max_length=2, choices=TARGET_MARKET)
    target_market = models.ManyToManyField(TargetMarket,verbose_name=_('target market'))
    # target_market = models.CommaSeparatedIntegerField(_('target_market'), max_length=10)
    budget = models.PositiveIntegerField(_('budget'))
    date_published = models.DateTimeField(_('date published'), default=timezone.now)

    # request need to man-verify
    is_verified = models.NullBooleanField(_('is verified'), blank=True, null=True,
                                          help_text=_('Can the request be verified?'))
    date_verified = models.DateTimeField(_('date verified'), default=timezone.now)
    provider_matched = models.PositiveIntegerField(_('provider matched'),default=0)

    provider_accepted = models.BooleanField(_('provider accepted'), default=False)
    # when he has select a provider,it has made a one-to-one to a provide, we should make a sign.
    # if he doesn't selected one after some time,we will delete it and the relations to Consult.
    provider_selected = models.BooleanField(_('provider selected'), default=False)

    request_cancelled = models.BooleanField(_('request cancelled'), default=False)

    # status = models.PositiveSmallIntegerField(_('status'), default=0)
    #0000 not verify
    #0001 verifid
    #0010 pushed
    #0100 accepted
    #1000 pay finished
    #10000 cancelled

    class Meta:
        # permissions = ()
        verbose_name = _('request')
        verbose_name_plural = _('request')

    # can it add price?
    def __unicode__(self):
        return "%d %s" % (self.id, self.request_title)
