# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import django.utils.timezone
from django.conf import settings


class Migration(migrations.Migration):

    dependencies = [
        ('account', '0001_initial'),
        migrations.swappable_dependency(settings.AUTH_USER_MODEL),
    ]

    operations = [
        migrations.CreateModel(
            name='Request',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('request_title', models.CharField(max_length='100', verbose_name='title')),
                ('request_detail', models.TextField(max_length=500, verbose_name='request detail')),
                ('budget', models.PositiveIntegerField(verbose_name='budget')),
                ('date_published', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date published')),
                ('is_verified', models.NullBooleanField(help_text='Can the request be verified?', verbose_name='is verified')),
                ('date_verified', models.DateTimeField(default=django.utils.timezone.now, verbose_name='date verified')),
                ('provider_matched', models.PositiveIntegerField(default=0, verbose_name='provider matched')),
                ('provider_accepted', models.BooleanField(default=False, verbose_name='provider accepted')),
                ('provider_selected', models.BooleanField(default=False, verbose_name='provider selected')),
                ('request_cancelled', models.BooleanField(default=False, verbose_name='request_cancelled')),
            ],
            options={
                'verbose_name': 'request',
                'verbose_name_plural': 'request',
            },
        ),
        migrations.CreateModel(
            name='Requester',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('amount_market_research', models.PositiveSmallIntegerField(default=0, verbose_name='Market Research')),
                ('amount_industry_consultation', models.PositiveSmallIntegerField(default=0, verbose_name='Industry Consultation')),
                ('amount_translation_service', models.PositiveSmallIntegerField(default=0, verbose_name='Translation Service')),
                ('amount_overseas_recruitment', models.PositiveSmallIntegerField(default=0, verbose_name='Overseas Recruitment')),
                ('amount_office_space_Rental', models.PositiveSmallIntegerField(default=0, verbose_name='Office Space Rental')),
                ('amount_legal_incorporation', models.PositiveSmallIntegerField(default=0, verbose_name='Legal & Incorporation')),
                ('amount_tax_accounting', models.PositiveSmallIntegerField(default=0, verbose_name='Tax & Accounting')),
                ('amount_business_travel', models.PositiveSmallIntegerField(default=0, verbose_name='Business Trip Planning')),
                ('account', models.OneToOneField(null=True, blank=True, to=settings.AUTH_USER_MODEL, verbose_name='account')),
            ],
            options={
                'verbose_name': 'requester',
                'verbose_name_plural': 'requester',
            },
        ),
        migrations.AddField(
            model_name='request',
            name='requester',
            field=models.ForeignKey(verbose_name='requester', to='request.Requester'),
        ),
        migrations.AddField(
            model_name='request',
            name='service_type',
            field=models.ForeignKey(verbose_name='service type', to='account.ServiceType'),
        ),
        migrations.AddField(
            model_name='request',
            name='target_market',
            field=models.ManyToManyField(to='account.TargetMarket', verbose_name='target market'),
        ),
    ]
