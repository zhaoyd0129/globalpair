from __future__ import unicode_literals
from django.contrib import admin
from .models import Request, Requester


# Register your models here.
# class RequestInline(admin.StackedInline):
#     model = Request


class RequesterAdmin(admin.ModelAdmin):
    list_display = ('id', 'account',
                    'amount_market_research',
                    'amount_industry_consultation',
                    'amount_translation_service',
                    'amount_overseas_recruitment',
                    'amount_office_space_Rental',
                    'amount_legal_incorporation',
                    'amount_tax_accounting',
                    'amount_business_travel')
    fields = ('account',
              ('amount_market_research', 'amount_industry_consultation',
               'amount_translation_service', 'amount_overseas_recruitment'),
              ('amount_office_space_Rental', 'amount_legal_incorporation',
               'amount_tax_accounting', 'amount_business_travel'),
    )


class RequestAdmin(admin.ModelAdmin):
    list_display = (
        'id',
        'requester',
        'request_title',
        'service_type',
        'request_detail',
        'budget',
        'date_published',
        'is_verified',
        'provider_matched',
        'provider_accepted',
        'provider_selected',
        'request_cancelled',
    )

admin.site.register(Requester, RequesterAdmin)
admin.site.register(Request, RequestAdmin)
